package be.kdg.java2;

import be.kdg.java2.domain.Course;
import be.kdg.java2.domain.Student;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<Student> students = new ArrayList<>();
//        students.add(new Student("jos", 1.92, LocalDate.of(1967,1,1)));
//        students.add(new Student("marie", 1.63, LocalDate.of(1977,1,1)));
//        students.add(new Student("francine", 1.17, LocalDate.of(1987,1,1)));
//        try {
//            ObjectOutputStream oos =
//                    new ObjectOutputStream(new FileOutputStream("objects/students.ser"));
//            oos.writeObject(students);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        Student jos = new Student("jos", 1.92, LocalDate.of(1967,1,1));
        Course java = new Course("Java", jos);
        jos.setCourse(java);
        try {
            ObjectOutputStream oos =
                    new ObjectOutputStream(new FileOutputStream("objects/students.ser"));
            oos.writeObject(jos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
