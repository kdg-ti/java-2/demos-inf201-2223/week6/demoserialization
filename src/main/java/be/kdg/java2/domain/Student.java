package be.kdg.java2.domain;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

public class Student implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private String name;
    private double size;
    private transient LocalDate birthday;
    private Course course;

    public Student(String name, double size, LocalDate birthday) {
        this.name = name;
        this.size = size;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", size=" + size +
                ", birthday=" + birthday +
                '}';
    }
}
