package be.kdg.java2.domain;

import java.io.Serializable;

public class Course implements Serializable {
    private String title;
    private Student student;

    public Course(String title, Student student) {
        this.title = title;
        this.student = student;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Course{" +
                "title='" + title + '\'' +
                '}';
    }
}
