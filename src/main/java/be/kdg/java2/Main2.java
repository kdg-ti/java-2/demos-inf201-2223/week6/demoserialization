package be.kdg.java2;

import be.kdg.java2.domain.Student;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class Main2 {
    public static void main(String[] args) {
//        try {
//            ObjectInputStream ois =
//                    new ObjectInputStream(new FileInputStream("objects/students.ser"));
//            List<Student> students = (List<Student>) ois.readObject();
//            students.forEach(System.out::println);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } catch (ClassNotFoundException e) {
//            throw new RuntimeException(e);
//        }
        try {
            ObjectInputStream ois =
                    new ObjectInputStream(new FileInputStream("objects/students.ser"));
            Student jos = (Student) ois.readObject();
            System.out.println(jos);
            System.out.println(jos.getCourse());
            System.out.println(jos.getCourse().getStudent());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
