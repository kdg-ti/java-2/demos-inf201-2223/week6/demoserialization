Demo Serialization
------------------
- Je kan objecten rechtstreeks naar harde schijf schrijven
- Ze moeten de Serializable interface implementeren
- Ze worden weggeschreven met een ObjectOutputStream (in Main)
- Je kan ze terug inladen met een ObjectInputStream (in Main2)
- Je kan velden negeren met transient keyword
- Circulaire dependencies vormen geen probleem
- Je kan zelf de serialVersionUID beheren: is die anders dan wordt het object niet geladen
- Je kan een collection ook serializeren, dat is wel handig!